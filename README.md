Le but de ce projet est de réaliser un petit jeu (le style de jeu est libre) en Javascript.
Vous devrez faire ce projet par équipe de deux en appliquant les méthodologies Agiles pour vous organiser.

Ce jeu devra être développé en utilisant la programmation orientée objet ainsi que les principes du MVC.
Le jeu n'utilisera pas de base de données. (si vous avez des informations à stocker entre plusieurs sessions de jeu, utilisez les localStorage)

Commencez par décider du type de jeu que vous voudrez faire, puis faire des maquettes de ce à quoi devra ressembler le jeu grosso modo avant de coder quoique ce soit.

Faire valider le type jeu par un formateur avant de vous lancer.